###################
Inventory System for TW8 and DW8 Merchandise
###################



* CodeIgniter 3.0 (as of this writing) - https://www.codeigniter.com/download

* Materialize CSS alpha release v0.97.6 (as of this writing) - http://materializecss.com/getting-started.html


To launch the system locally:

1. Start Apache and MySQL in your cross-platform web server solution software stack (XAMPP, WAMP, LAMP, etc)
2. Go to localhost/tnd