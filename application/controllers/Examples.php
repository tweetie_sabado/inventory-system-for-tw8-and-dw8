<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examples extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	//EXAMPLES

	public function invoice()
	{
		$data['main_content'] = 'examples/invoice';
		$this->load->view('includes/templates/template', $data);
	}

	public function print_invoice()
	{
		$data['main_content'] = 'examples/print_invoice';
		$this->load->view('includes/templates/template_print', $data);
	}

	public function login()
	{
		$data['main_content'] = 'examples/login';
		$this->load->view('includes/templates/template_login', $data);
	}

	public function register()
	{
		$data['main_content'] = 'examples/register';
		$this->load->view('includes/templates/template_register', $data);
	}

  public function lockscreen()
  {
    $data['main_content'] = 'examples/lockscreen';
    $this->load->view('includes/templates/template', $data);
  }

  public function error_404()
  {
    $data['main_content'] = 'examples/error_404';
    $this->load->view('includes/templates/template', $data);
  }

  public function error_500()
  {
    $data['main_content'] = 'examples/error_500';
    $this->load->view('includes/templates/template', $data);
  }

  public function blank()
  {
    $data['main_content'] = 'examples/blank';
    $this->load->view('includes/templates/template', $data);
  }

  public function pace()
  {
    $data['main_content'] = 'examples/pace';
    $this->load->view('includes/templates/template', $data);
  }
}
