<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailbox extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	//MAILBOX

	public function index()
	{
		$data['main_content'] = 'mailbox';
		$this->load->view('includes/templates/template_mailbox', $data);
	}

	public function compose()
	{
		$data['main_content'] = 'compose';
		$this->load->view('includes/templates/template_compose', $data);
	}

}
