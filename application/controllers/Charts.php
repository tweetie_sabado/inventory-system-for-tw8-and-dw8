<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charts extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	//CHARTS

	public function chartsjs()
	{
		$data['main_content'] = 'charts/chartsjs';
		$this->load->view('includes/templates/template_charts', $data);
	}

	public function morris()
	{
		$data['main_content'] = 'charts/morris';
		$this->load->view('includes/templates/template_charts', $data);
	}

	public function flot()
	{
		$data['main_content'] = 'charts/flot';
		$this->load->view('includes/templates/template_charts', $data);
	}

	public function inline()
	{
		$data['main_content'] = 'charts/inline';
		$this->load->view('includes/templates/template_charts', $data);
	}
}
