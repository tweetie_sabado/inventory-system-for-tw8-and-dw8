<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UIElements extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

   //UI ELEMENTS

   public function general()
   {
     $data['main_content'] = 'ui_elements/general';
     $this->load->view('includes/templates/template', $data);
   }

   public function icons()
   {
     $data['main_content'] = 'ui_elements/icons';
     $this->load->view('includes/templates/template', $data);
   }

   public function buttons()
   {
     $data['main_content'] = 'ui_elements/buttons';
     $this->load->view('includes/templates/template', $data);
   }

   public function sliders()
   {
     $data['main_content'] = 'ui_elements/sliders';
     $this->load->view('includes/templates/template_slider', $data);
   }

   public function timeline()
   {
     $data['main_content'] = 'ui_elements/timeline';
     $this->load->view('includes/templates/template', $data);
   }

   public function modals()
   {
     $data['main_content'] = 'ui_elements/modals';
     $this->load->view('includes/templates/template', $data);
   }


}
