
<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>design_template/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>design_template/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>design_template/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>design_template/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>design_template/dist/js/demo.js"></script>
<!-- jQuery Knob -->
<script src="<?php echo base_url();?>design_template/plugins/knob/jquery.knob.js"></script>

<!-- CHARTS -->
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url();?>design_template/plugins/chartjs/Chart.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>design_template/plugins/morris/morris.min.js"></script>
<!-- FLOT CHARTS -->
<script src="<?php echo base_url();?>design_template/plugins/flot/jquery.flot.min.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo base_url();?>design_template/plugins/flot/jquery.flot.resize.min.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?php echo base_url();?>design_template/plugins/flot/jquery.flot.pie.min.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="<?php echo base_url();?>design_template/plugins/flot/jquery.flot.categories.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>design_template/plugins/sparkline/jquery.sparkline.min.js"></script>

 <!-- PAGE SCRIPTS -->
 <?php
  if($main_content == 'charts/chartsjs'){
    $this->load->view('page_scripts/chartsjs_script');
  }elseif($main_content == 'charts/morris'){
    $this->load->view('page_scripts/morris_script');
  }elseif($main_content == 'charts/flot'){
    $this->load->view('page_scripts/flot_script');
  }elseif($main_content == 'charts/inline'){
    $this->load->view('page_scripts/inline_script');
  }
 ?>
</body>
</html>
